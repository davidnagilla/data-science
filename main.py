import legal_doc as ld

x, y = ld.pre()
# Let us consider CountVectorizer from sklearn
# CountVectorizer, TfidfVectorizer, Word Embeddings, Word2Vec
# X_vec = ld.countVectorizer(x)
X_vec = ld.tfidfVectorizer(x)
y_encoded = ld.labelEncoding(y)
# Now, the dataset should be split in to train and test sets
X_train, X_test, y_train, y_test = ld.splitTestTrain(X_vec, y_encoded)
#kfold validation
#X_train, X_test, y_train, y_test = kFold(X_vec, y_encoded)
#plot Labels of Dataset
# ld.plotLabels(y)
#precison, recall and f-measure of all the six classifiers
naiveBayesPrecision, naiveBayesRecall, naiveBayesFMeasure = ld.applyNaiveBayesClassifier(X_train, y_train, X_test, y_test)
#svmPrecision, svmRecall, svmFMeasure = ld.applySVMClassifier(X_train, y_train, X_test, y_test)
#randomForestPrecision, randomForestRecall, randomForestFMeasure = ld.applyRandomForestClassifier(X_train, y_train, X_test, y_test)
logisticRegressionPrecision, logisticRegressionRecall, logisticRegressionFMeasure = ld.applyLogisticRegressionClassifier(X_train, y_train, X_test, y_test)
sgdPrecision, sgdRecall, sgdFMeasure = ld.applySGDClassifier(X_train, y_train, X_test, y_test)
#decisionTreePrecision, decisionTreeRecall, decisionTreeFMeasure = ld.applyDecisionTreeClassifier(X_train, y_train, X_test, y_test)
#Plot Precision-Recall comparison graph
# ld.plotPreRec(naiveBayesRecall, naiveBayesPrecision, svmRecall, svmPrecision, randomForestRecall, randomForestPrecision) #logisticRegressionRecall, logisticRegressionPrecision, sgdRecall, sgdPrecision)
# #plot FMeasure comparison graph
# ld.plotAcuuracyComaprisonGraph(naiveBayesFMeasure, svmFMeasure, randomForestFMeasure)#, logisticRegressionFMeasure, sgdFMeasure)
