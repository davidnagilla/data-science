import datetime

import time
import re
import ast
import json
from ast import literal_eval

import nltk
import string
from nltk.corpus import stopwords
from nltk.stem import PorterStemmer
from nltk.corpus import stopwords

    
import re

from geopy.geocoders import Nominatim
from geopy.exc import GeocoderTimedOut

outputFile = open('labelled/output_152.csv','w')

outputFile.write('label,text,location\n')

#count = 0

with open('data/output_152.txt','r', encoding="ascii", errors="ignore") as fh:
  for line in fh:
   tweet = literal_eval(line)
   
   if tweet['lang']=='en':

        #wordList = re.sub("[^\w]", " ",  mystr).split()
        data = ' '.join([word for word in tweet['text'].split() if word.isalpha()])
        
        tweet['text']=tweet['text'].replace(',','')
        data = tweet['text']+'\n'
        
        tweet['text'] = tweet['text'].lower()
        cachedStopWords = stopwords.words("english")
        
        tweet['text'] = re.sub(r'[^\w\s]','',tweet['text'])
        tweet['text'] = re.sub(r"http\S+", "", tweet['text'])
        
        data = ' '.join([word for word in tweet['text'].split() if word not in cachedStopWords]) 

        b="" 
        date_1 = tweet['created_at']
        d = datetime.datetime.strptime(date_1, '%a %b %d %H:%M:%S %z %Y')
        d = d.strftime('%Y-%m-%d')
        start = datetime.date(2014, 6, 10)
        start =  str(start)
        end = datetime.date(2014, 7, 13)
        end = str(end)
        if d < end and d > start:
            
            
            if tweet['coordinates']== None:
                
                wordList = re.sub("[^\w]", " ",  data).split()
                #print(wordList)
                keyword_list = ['attend', 'attending', 'watch', 'watching']
                if any(word in wordList for word in keyword_list):
                 b="Non-geo"
                 data = 'Attending' + ',' + data + ',' + b + '\n'
                 outputFile.write(data)
                 print(data)
            if tweet['coordinates']!= None:
                
                lat = tweet['coordinates']
                lon = lat['coordinates']
                geolat = str(lon[1])
                geolong = str(lon[0])
                latlong = [geolat, geolong]
                val = ", ".join(latlong)
                geolocator = Nominatim()
                try:
                    location = geolocator.reverse(val,timeout=2000, language='en')
                    address = location.raw
                    b = address['address']['country']
                    if b == 'Brazil' :
                        data = 'Attending' + ',' + data + ',' + b
                        #outputFile.write(data)
                        print(data)
                    else:
                        data = 'Not Attending' + ',' + data + ',' + b
                        #outputFile.write(data)
                        print(data)
                    data= data +'\n'
                    if data!='\n':
                        outputFile.write(data)
                except GeocoderTimedOut as e:
                   print("Error: geocode failed on input %s with message %s"%(b,e))
           
                    
outputFile.close()
